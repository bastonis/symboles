# Symboles

## Description

This repository contains all the symbols use by the game ``Baston''.

## Authors

The Authors of each picture should soon arrive.

- Arc.png :
- AttaqueSournoise.png :
- BatonMagique.png :
- Blaster.png :
- Bloque.png : 
- Bouclier.png : 
- Casque.png :
- Collision.png :
- Couronne.png :
- Credits.png : 
- Crocs.png :
- Devastator.png :
- DragonBall1.png : [PngFind.com](https://www.pngfind.com/)
- DragonBall2.png : [PngFind.com](https://www.pngfind.com/)
- DragonBall3.png : [PngFind.com](https://www.pngfind.com/)
- DragonBall4.png : [PngFind.com](https://www.pngfind.com/)
- Eblouissement.png :
- Ecrasement.png :
- Epee.png :
- Etendard.png :
- Fanatique.png :
- Flamme.png :
- FlopFlop.png : 
- ForceLumineuse.png :
- ForceObscure.png :
- Fuite.png :
- FusilElite.png :
- Gel.png :
- Hurlante.png :
- Killer.png :
- MagieDemoniaque.png :
- MagieIncantatoire.png :
- MagieNecromantique.png :
- Medic.png :
- Necromancie.png :
- Null.png : [@Ultraxime](https://gitlab.com/Ultraxime)
- Oeil.png :
- Paix.png :
- Paralyseur.png :
- Pitie.png :
- Poison.png :
- Portail.png :
- Prescience.png :
- Providence.png :
- RazDeMaree.png :
- Retourne.png : 
- Sablier.png :
- SabreBleu.png :
- SabreRouge.png :
- SabreVert.png :
- SabreViolet.png :
- Sorcellerie.png : 
- Tenebre.png : [@Ultraxime](https://gitlab.com/Ultraxime)
- Tornade.png :
- Traitre.png : 
- Wakfu.png : 
- Zizanie.png :


## License

All the images contained here are still owned by their original owner.